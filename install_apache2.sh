#! /bin/bash
# Configuration - To be called from the Dockerfile

### APACHE 2 ###

# Manipulate envvars
sed -i "/APACHE_RUN_USER/d" /etc/apache2/envvars
sed -i "/APACHE_RUN_GROUP/d" /etc/apache2/envvars
sed -i "/APACHE_LOCK_DIR/d" /etc/apache2/envvars
sed -i "/APACHE_LOG_DIR/d" /etc/apache2/envvars
sed -i "/APACHE_PID_FILE/d" /etc/apache2/envvars
sed -i "/APACHE_RUN_DIR/d" /etc/apache2/envvars
printf "\nexport APACHE_RUN_USER=container\n" >> /etc/apache2/envvars
printf "export APACHE_RUN_GROUP=container\n" >> /etc/apache2/envvars
printf "export APACHE_LOCK_DIR=/home/container/apache2/lock\n" >> /etc/apache2/envvars
printf "export APACHE_LOG_DIR=/home/container/apache2/logs\n" >> /etc/apache2/envvars
printf "export APACHE_PID_FILE=/home/container/apache2/apache2.pid\n" >> /etc/apache2/envvars
printf "export APACHE_RUN_DIR=/home/container/apache2/run\n" >> /etc/apache2/envvars

# Enable mod_rewrite
sed -i '/LoadModule rewrite_module/s/^#//g' /usr/local/apache2/conf/httpd.conf

# Create placeholders
mkdir -p /home/container/apache2
touch /home/container/apache2/site.conf
touch /home/container/apache2/ports.conf

# symlink | Site configuration
rm -rf /etc/apache2/sites-enabled/*
rm -rf /etc/apache2/sites-available/*
ln -s /home/container/apache2/site.conf /etc/apache2/sites-enabled/site.conf

# symlink | Port configuration
rm /etc/apache2/ports.conf
ln -s /home/container/apache2/ports.conf /etc/apache2/ports.conf

# Remove placeholders
rm -rf /home/container/apache2

# Permissions (We are root)
chown -R container /etc/apache2
chmod -R 750 /etc/apache2

### PHP ###

# Update the PHP.ini file, enable <? ?> tags and quieten logging.
sed -i "s/short_open_tag = Off/short_open_tag = On/" /etc/php/7.1/apache2/php.ini
sed -i "s/error_reporting = .*$/error_reporting = E_ERROR | E_WARNING | E_PARSE/" /etc/php/7.1/apache2/php.ini
sed -i 's#;session.save_path = .*$#session.save_path = "/home/container/php/sessions"#' /etc/php/7.1/apache2/php.ini

a2enmod ssl
a2enmod rewrite
