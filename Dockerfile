FROM ubuntu:16.04

MAINTAINER FearNixx Technik, <technik@fearnixx.de>

# Basic system setup

RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get -y install \
	curl ca-certificates openssl git tar unzip bash gcc software-properties-common
	
RUN useradd -m -d /home/container -s /bin/bash container

# Apache2 setup
	
RUN LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php
RUN apt-get update \
	&& apt-get -y install \
	apache2 \
	php7.1 \
	php7.1-bcmath \
	php7.1-gd \
	php7.1-tokenizer \
	php7.1-mcrypt \
	php7.1-apcu \
	php7.1-cli \
	php7.1-mysql \
	php7.1-pdo \
	php7.1-mbstring \
	php7.1-xml \
	php7.1-curl \
	php7.1-zip

# Configurations | Apache2 | PHP
RUN curl -o install_apache2.sh https://gitlab.com/fearnixxgaming/docker/apache2/raw/master/install_apache2.sh \
	&& chmod +x install_apache2.sh
RUN ./install_apache2.sh

USER container
ENV USER container
ENV HOME /home/container

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]
