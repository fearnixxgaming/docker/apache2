#!/bin/bash
# TS3Bot installation script
# (Put in Pterodactyl egg)
#
# Server Files: /mnt/server

cd /mnt/server

mkdir -p apache2/logs
mkdir -p apache2/lock
mkdir -p apache2/run
mkdir -p htdocs
printf "<?php\nphpinfo();" > htdocs/phpinfo.php
printf "Order allow,deny\nAllow from all\nSatisfy any\n" > htdocs/.htaccess

mkdir -p php/sessions