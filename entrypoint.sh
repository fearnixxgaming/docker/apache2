#!/bin/bash
# /entrypoint.sh
# # Default entrypoint file from Pterodactyl # #
sleep 5

cd /home/container

# Delete last .pid file
if [ -f /home/container/apache2/apache2.pid ]; then
	rm /home/container/apache2/apache2.pid
fi

if [ -z "$SERVER_PORT" ]; then
	SERVER_PORT="8000"
fi

if [ -z "$SERVER_NAME" ]; then
	SERVER_NAME="example.com"
fi

if [ -z "$SERVER_ROOT" ]; then
	SERVER_ROOT="/home/container/htdocs"
fi

# Rewrite site.conf on each start with the new information.
SITECONF=""
SITECONF="${SITECONF}<VirtualHost *:${SERVER_PORT}>\nServerName ${SERVER_NAME}\nDocumentRoot ${SERVER_ROOT}\n"
SITECONF="${SITECONF}CustomLog /home/container/apache2/logs/site-access.log combined\nErrorLog /home/container/apache2/logs/site-error.log\n"
if [ -f /home/container/ssl/privkey.pem ] && [ -f /home/container/ssl/cacert.pem ] && [ -f /home/container/ssl/fullchain.pem ]; then
	SITECONF="${SITECONF}SSLEngine On\n"
	SITECONF="${SITECONF}SSLCertificateFile /home/container/ssl/cacert.pem\n"
	SITECONF="${SITECONF}SSLCertificateKeyFile /home/container/ssl/privkey.pem\n"
	SITECONF="${SITECONF}SSLCertificateChainFile /home/container/ssl/fullchain.pem\n"
fi
SITECONF="${SITECONF}<Directory> \"${SERVER_ROOT}\">\nAllowOverride All\n</Directory>\n"
if [ ! -z "$SITECONF_EXTRA" ]; then
	SITECONF="${SITECONF}${SITECONF_EXTRA}"
fi
SITECONF="${SITECONF}</VirtualHost>\n"

printf "$SITECONF" > /home/container/apache2/site.conf

# Add port from environment vars
printf "Listen ${SERVER_PORT}\n" > /home/container/apache2/ports.conf

# Source ENVVARS
if [ -f /etc/apache2/envvars ]; then
	. /etc/apache2/envvars
fi
if [ -f envvars ]; then
	. ./envvars
fi

echo "###########################################"
ls --color=auto -l /etc/apache2
ls --color=auto -l /etc/apache2/sites-enabled
echo "###########################################"

# Replace Startup Variables
MODIFIED_STARTUP=`eval echo $(echo ${STARTUP} | sed -e 's/{{/${/g' -e 's/}}/}/g')`
echo ":/home/container$ ${MODIFIED_STARTUP}"

# Run the Server
# Tipp: apache2 -D FOREGROUND
${MODIFIED_STARTUP}